#!/bin/bash

grep 'TDC 1\|threshold =' noiseScanResults_UCLSupply.txt > tmp.txt
perl -0pe 's/\nTDC/ TDC/g' < tmp.txt > tmp2.txt
rm tmp.txt
cat tmp2.txt | cut -d ' ' -f10,16 > TDC1.txt
rm tmp2.txt
