Made Faraday cage box from copper plates and mesh. Do noise scan with box closed.

Bench top power supply used as UCL's supply seems to be noisy - it creates hits at very high thresholds.

Data taken through slow control interface.

Only one TDC present using TDC motherboard with termination resistors put on by hand.
