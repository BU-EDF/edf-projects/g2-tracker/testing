First attempt at a noise scan on whole chain.

Bench top power supply used as UCL's supply seems to be noisy - it creates hits at very high thresholds.

Data taken through slow control interface.

ASDQs simply sat on bench - no attempt made to shield.

TDC motherboard termination resistors not present, so hits are 0-3.3V going into the FPGA.
