#!/bin/bash

grep 'TDC 2\|threshold =' noiseScanResults_2015_04_21.txt > tmp.txt
perl -0pe 's/\nTDC/ TDC/g' < tmp.txt > tmp2.txt
rm tmp.txt
cat tmp2.txt | cut -d ' ' -f10,16 > TDC2.txt
rm tmp2.txt
