Made Faraday cage box from copper plates and mesh.  First do noise scan with lid open to get an idea what difference it'll make.

Bench top power supply used as UCL's supply seems to be noisy - it creates hits at very high thresholds.

Data taken through slow control interface.

Only one TDC present using TDC motherboard with termination resistors put on by hand.
